#!/usr/bin/python
# coding: utf-8

# This file is part of mate-accessibility.
#
#    mate-accessibility is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by the Free
#    Software Foundation, either version 3 of the License, or (at your option) any
#    later version.
#
#    mate-accessibility is distributed in the hope that it will be useful, but
#    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
#    details.
#
#    You should have received a copy of the GNU General Public License along
#    with mate-accessibility.  If not, see <http://www.gnu.org/licenses/>.


import sys
import os
from distutils.version import LooseVersion

from base_install import Accessibility_Installer

class SpeechDFrench_Installer(Accessibility_Installer):
    usage = "usage: %s action\nactions are:\n\tinstall\n\tuninstall" % sys.argv[0]

    def __init__(self):
        Accessibility_Installer.__init__(self)
        self.pkg_name = "mate-accessibility-full-fr"

    def install(self):
        self.replace_line_begin_by("/etc/speech-dispatcher/speechd.conf",
                                   'DefaultLanguage', '# DefaultLanguage "en"')

    def uninstall(self):
        pass

    def upgrade(self):
        if LooseVersion(self.last_configured_version) <= LooseVersion('0.12.0'):
            self.install()

if __name__ == "__main__":
    inst = SpeechDFrench_Installer()
    inst.launch(sys.argv)
