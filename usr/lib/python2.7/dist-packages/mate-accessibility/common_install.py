#!/usr/bin/python
# coding: utf-8

# This file is part of mate-accessibility.
#
#    mate-accessibility is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by the Free
#    Software Foundation, either version 3 of the License, or (at your option) any
#    later version.
#
#    mate-accessibility is distributed in the hope that it will be useful, but
#    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
#    details.
#
#    You should have received a copy of the GNU General Public License along
#    with mate-accessibility.  If not, see <http://www.gnu.org/licenses/>.


import sys
import os
from distutils.version import LooseVersion

from base_install import Accessibility_Installer

class Core_Accessibility_Installer(Accessibility_Installer):
    usage = "usage: %s action\nactions are:\n\tinstall :\t install mate-a11y setup\n\tuninstall :\t uninstall mate-a11y setup" % sys.argv[0]
    def __init__(self):
        self.pkg_name = "mate-accessibility-common" # to get changelog
        self.version = None
        self.available_desktop_shortcuts = []

    def teardown_common_aliases(self):
        user = self.get_first_system_user()
        commands_to_execute = [
                'rm -f /home/%s/.bash_aliases' %user,
                'rm -rf /home/%s/.bash_aliases.d' %user
                ]
        self.execute_commands(commands_to_execute)

    def install(self):
        pass

    def uninstall(self):
        pass

    def upgrade(self):
        # if the settings are yet installed
        if LooseVersion(self.last_configured_version) >= LooseVersion('0.7.0'):
            return
        self.teardown_common_aliases()

if __name__ == "__main__":
    inst = Core_Accessibility_Installer()
    inst.launch(sys.argv)
