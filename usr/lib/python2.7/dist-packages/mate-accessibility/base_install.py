#!/usr/bin/python
# coding: utf-8

# This file is part of mate-accessibility.
#
#    mate-accessibility is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by the Free
#    Software Foundation, either version 3 of the License, or (at your option) any
#    later version.
#
#    mate-accessibility is distributed in the hope that it will be useful, but
#    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
#    details.
#
#    You should have received a copy of the GNU General Public License along
#    with mate-accessibility.  If not, see <http://www.gnu.org/licenses/>.

import sys
import os
import stat
import locale
import commands
import shutil
import ConfigParser
import json
import logging
import pwd
import subprocess

class Accessibility_Installer():
    usage = "usage: %s action\nactions are:\n\tinstall :\t install mate-a11y setup\n\tconfigure :\t alias ot install\n\tuninstall :\t uninstall mate-a11y setup" % sys.argv[0]
    def __init__(self):
        self.version = None

    def launch(self, argv):
        action_list = ["configure", "remove", "upgrade", "failed-upgrade", "abort-upgrade"]
        try:
            action = None
            for i in xrange(len(argv)):
                if argv[i] in action_list:
                    action = argv[i]
            if not action:
                print self.usage
                os._exit(1) # cause we're in a try
            last_configured_version = None
            if len(argv) > 2:
                last_configured_version = argv[2]
        except:
            print self.usage
            print "got: sys.argv = ", sys.argv
            sys.exit(1)
        if action == "configure":
            if last_configured_version:
                try:
                    func_upgrade = getattr(self, "upgrade")
                # mean upgrade function doesn't exist
                except:
                    return
                self.last_configured_version = last_configured_version
                func_upgrade()
            else:
                self.install()
        elif action == "remove":
            self.uninstall()
        elif action == "upgrade":
            pass
        elif action == "failed-upgrade":
            self.fail_upgrade()
        elif action == "abort-upgrade":
            self.abort_upgrade()

    def get_version(self):
        for arg in sys.argv[1:]:
            if arg.startswith("--version="):
                self.version = arg[10:]
        if not self.version or self.version == "":
            self.version = commands.getoutput('zcat /usr/share/doc/%s/changelog*.gz | head -n1 | cut -d "(" -f 2 | cut -d ")" -f 1' %self.pkg_name)
        if not self.version or self.version == "" or self.version == "gzip":
            logging.error("Error: no version, or bad version found %s", self.version)
            sys.exit(1)

    def fail_upgrade(self):
        pass

    def abort_upgrade(self):
        print "something went wrong, please contact your system's administrator"

    def execute_commands(self, commands):
        for cmd_line in commands:
            # escaping "
            cmd_line = cmd_line.replace('"', '\\"')
            cmd_line = cmd_line.replace('`', '\\`')
            #print '[DEBUG] executing: bash -c "%s"' %cmd_line
            if os.system('bash -c "%s"' %cmd_line):
                print "Error: unable to confirm correct execution of bash -c \"%s\"" %cmd_line

    def get_non_system_users(self):
        min_uid = 1000  # for proper uids:
        max_uid = 60000 #grep "^UID_" /etc/login.defs
        users = pwd.getpwall()
        non_system_users = []
        for user in users:
            user_uid = user.pw_uid
            if int(user_uid) < min_uid or int(user_uid) > max_uid:
                continue
            non_system_users += [[user_uid, user]] # put uid first to sort

        # now, to support users that don't have a pw entry (e.g. SSSD/LDAP
        # setup at Enercoop), we list directories in /home and try to piece
        # together meaningful info from there
        home_users = []
        home_base = '/home'  # for proper value, check DHOME in /etc/adduser.conf
        for name in os.listdir(home_base):
            home_path = os.path.join(home_base, name)
            home_stat = os.stat(home_path)
            # ignore root-owned entries like lost+found, and all users < 1000
            if home_stat.st_uid < min_uid:
                continue
            # ignore non-directories
            if not stat.S_ISDIR(home_stat.st_mode):
                continue
            # make sure we don't already have a better entry for this
            for ns_uid, ns_pwd in non_system_users:
                if (ns_uid == home_stat.st_uid or
                    ns_pwd.pw_name == name):
                    break
            else:
                # mock up a pwd entry the best we can
                entry = pwd.struct_passwd((name, 'x', home_stat.st_uid,
                                           home_stat.st_gid, ',,,', home_path,
                                           '/bin/sh'))
                home_users += [[entry.pw_uid, entry]]

        # Sort the list py UID, but put the entries made up from homedirs
        # at the end because they are less trustworthy and many callers only
        # use the first entry from the list
        non_system_users = sorted(non_system_users) + sorted(home_users)

        # filter out some users
        filtered_users = [
            'enercoop'  # asked by Enercoop SI on 2023-06-07
        ]
        return [e for e in non_system_users
                if e[1].pw_name not in filtered_users]

    def get_first_system_user(self):
        try:
            first_user = self.get_non_system_users()[0]
            return self.get_username(first_user)
        except:
            return None

    def get_username(self, user):
        return user[1].pw_name

    def get_first_user_home(self):
        return self.get_user_home(self.get_first_system_pwd_user())

    def get_first_system_pwd_user(self):
        try:
            return self.get_non_system_users()[0][1]
        except:
            return None


    def get_user_home(self, pwd_user):
        home_folder = pwd_user.pw_dir
        if not os.path.exists(home_folder):
            os.makedirs(home_folder)
            os.system("chown %s:%s '%s/'" %(pwd_user.pw_uid, pwd_user.pw_gid, home_folder))
        return home_folder

    def get_first_user_desktop(self):
        pwd_user = self.get_first_system_pwd_user()
        if not pwd_user:
            print "no current user"
            return None
        return self.get_user_desktop_dir(pwd_user)

    def get_user_desktop_dir(self, pwd_user):
        home_folder =  self.get_user_home(pwd_user)
        old_home =  os.environ["HOME"]
        os.environ["HOME"] = home_folder
        desktop_folder = subprocess.check_output(['xdg-user-dir', 'DESKTOP']).rstrip()
        os.environ["HOME"] = old_home
        if not os.path.exists(desktop_folder):
            os.makedirs(desktop_folder)
            os.system("chown %s:%s '%s/'" %(pwd_user.pw_uid, pwd_user.pw_gid, desktop_folder))
        return desktop_folder

    def setup_desktop(self):
        pwd_user = self.get_first_system_pwd_user()
        if not pwd_user:
            return
        desktop_folder = self.get_user_desktop_dir(pwd_user)
        self.add_shortcuts_to_desktop(pwd_user, desktop_folder)

    def add_shortcut_to_desktop(self, current_user, desktop_folder, shortcut, name=None, replace=False):
        """ Installs `shortcut` from /etc/skel/Desktop/ on `current_user`'s
        `desktop_folder`, possibly under `name`.  If `name` is None (default),
        it uses the original name `shortcut`.  Replaces an existing file only
        if `replace` is True. """

        if isinstance(current_user, pwd.struct_passwd):
            pw_uid, pw_gid = current_user.pw_uid, current_user.pw_gid
        else:
            for ns_uid, ns_pwd in self.get_non_system_users():
                if ns_pwd.pw_name == current_user:
                    pw_uid, pw_gid = ns_pwd.pw_uid, ns_pwd.pw_gid
                    break
            else:
                # not found, hope that there's a group by the user's name
                pw_uid, pw_gid = current_user, current_user

        new_desktop_shortcut = os.path.join(desktop_folder, name or shortcut)
        if replace or not os.path.exists(new_desktop_shortcut):
            shutil.copy(os.path.join("/etc/skel/Desktop/", shortcut), new_desktop_shortcut)
            os.system("chown %s:%s '%s'" %(pw_uid, pw_gid, new_desktop_shortcut))
            if shortcut.endswith(".desktop"):
                os.system("chmod +x '%s'" %(new_desktop_shortcut))

    def add_shortcuts_to_desktop(self, current_user, desktop_folder, replace_existing_file=False):
        for fil in self.available_desktop_shortcuts:
            self.add_shortcut_to_desktop(current_user, desktop_folder, fil, replace=replace_existing_file)

    def teardown_desktop(self):
        current_user = self.get_first_system_user()
        if not current_user:
            return
        desktop_folder = self.get_first_user_desktop()
        for fil in self.available_desktop_shortcuts:
            try:
                os.remove(os.path.join(desktop_folder, fil))
            except: # don't fail if user has moved or deleted the file
                print "note: didn't find file %s to delete" %fil
                pass

    # browses a sample path (eg: /etc/skel) and re-creates the files, potentially merging them
    # format can be "conf" for classical .conf files, or "json"
    def json_merge_tree(self, path, ref_path, override_values=False, formatt="conf"):
        if formatt == "conf":
            parser = RawConfigParser() # instanciate parser
            ref_parser = RawConfigParser() # instanciate parser for reference files
        logging.debug("beginning merge of %s, from %s", path, ref_path)
        for basedir, subdirs, files in os.walk(ref_path):
            logging.debug("walking folder: %s", basedir)
            for fil in files:
                ref_file_path = os.path.join(basedir, fil) # full path of reference
                target_path = os.path.join(path, basedir[len(ref_path) - 1 :], fil)
                logging.debug("test: %s", len(ref_path))
                logging.debug("test: %s", basedir[len(ref_path) - 1 :])
                if not os.path.exists(target_path):
                    if not os.path.exists(os.path.split(target_path)[0]):
                        logging.debug("creating folder(s) %s", os.path.split(target_path)[0])
                        os.makedirs(os.path.split(target_path)[0])
                    logging.debug("copying sample %s to %s" %(fil, basedir))
                    shutil.copy(ref_file_path, target_path)
                elif override_values:
                    logging.info("file %s already present, backing up, then merging", target_path)
                    shutil.copy(target_path, "%s.backup" %target_path)
                    if formatt == "conf":
                        parser.read(target_path)
                        ref_parser.read(ref_file_path)
                        for section in ref_parser.section():
                            if not parser.has_section(section): parser.add_section(section)
                            for option, value in ref_parser.items(section):
                                if not parser.has_option(option): parser.add_option(option)
                                parser.set(section, option, value)

                    elif formatt == "json":
                        with open(ref_file_path, 'r') as fil:
                            ref_var = json.load(fil)
                        with open(target_path, 'r') as fil:
                            var = json.load(fil)
                        var.update(ref_var)
                        # write the file again
                        with open(target_path, 'w') as fil:
                            json.dump(var, fil, indent=4)
                    else:
                        logging.error("Unknown config file format: %s", formatt)
                else:
                    logging.debug("not overriding file %s" %target_path)

     # TODO add --override in tweak mate-acce-configure

    def replace_line_begin_by(self, filename, start_with, new_line):
        with open(filename, 'r+') as f:
            lines = f.readlines()
            isFound = False
            f.seek(0)
            f.truncate()
            for line in lines:
                if not isFound and line.startswith(start_with):
                    line = new_line + '\n'
                    isFound = True
                f.write(line)
        return isFound

    def check_string_is_in_file(self, path, search_string):
        with open(path, 'r') as f:
            for line in f.readlines():
                if search_string in line:
                    return True
        return False
