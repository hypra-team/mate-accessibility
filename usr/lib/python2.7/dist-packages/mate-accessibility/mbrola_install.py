#!/usr/bin/python
# coding: utf-8

# This file is part of mate-accessibility.
#
#    mate-accessibility is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by the Free
#    Software Foundation, either version 3 of the License, or (at your option) any
#    later version.
#
#    mate-accessibility is distributed in the hope that it will be useful, but
#    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
#    details.
#
#    You should have received a copy of the GNU General Public License along
#    with mate-accessibility.  If not, see <http://www.gnu.org/licenses/>.


import sys
from distutils.version import LooseVersion

from base_install import Accessibility_Installer

class Mbrola_Installer(Accessibility_Installer):
    usage = "usage: %s action\nactions are:\n\tinstall\n\tuninstall" % sys.argv[0]

    def __init__(self):
        Accessibility_Installer.__init__(self)
        self.pkg_name = "mate-accessibility-mbrola"
        self.speechd_conf = "/etc/speech-dispatcher/speechd.conf"

    def install(self):
        commands_to_execute = [
            'sed -i "s/^DefaultModule\ espeak-ng$/DefaultModule espeak-mbrola-generic/" %s' % self.speechd_conf
        ]
        self.execute_commands(commands_to_execute)

    def upgrade(self):
        # comment-out when upgrading as we don't need it anymore
        if LooseVersion(self.last_configured_version) < LooseVersion('1.4.1'):
            commands_to_execute = [
                'sed -i "s/^AddModule .espeak-mbrola-generic./#&/" %s' % self.speechd_conf,
                'sed -i "s/^DefaultModule\ espeak-ng$/DefaultModule espeak-mbrola-generic/" %s' % self.speechd_conf
            ]
            self.execute_commands(commands_to_execute)

    def uninstall(self):
        commands_to_execute = [
            'sed -i "s/^\(AddModule .espeak-mbrola-generic.\)/#\\1/" %s' % self.speechd_conf,
            'sed -i "s/^DefaultModule\ espeak-mbrola-generic/DefaultModule espeak-ng/" %s' % self.speechd_conf
        ]
        self.execute_commands(commands_to_execute)

if __name__ == "__main__":
    inst = Mbrola_Installer()
    inst.launch(sys.argv)
