#!/usr/bin/python
# coding: utf-8

# This file is part of mate-accessibility.
#
#    mate-accessibility is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by the
#    Free Software Foundation, either version 3 of the License, or (at your
#    option) any later version.
#
#    mate-accessibility is distributed in the hope that it will be useful, but
#    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
#    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#    for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with mate-accessibility.  If not, see <http://www.gnu.org/licenses/>.

import os
import ConfigParser
import subprocess


class UserSessionOneShot(object):
    """ A base class for one-shot script runs under the all users session
        implemented via XDG autostart feature """

    def __init__(self, desktop_id):
        self.desktop_id = desktop_id

    def _run(self):
        pass

    def run(self):
        self._run()
        self._disable_autostart()

    def _disable_autostart(self):
        if 'HOME' not in os.environ:
            return

        autostart_directory = os.path.join(os.environ['HOME'],
                                           '.config/autostart/')
        try:
            os.makedirs(autostart_directory)
        except OSError as e:
            from errno import EEXIST
            if e.errno != EEXIST:
                raise e

        old_desktop_file = '/etc/xdg/autostart/' + self.desktop_id + '.desktop'
        new_desktop_file = os.path.join(autostart_directory,
                                        self.desktop_id + '.desktop')

        parser = ConfigParser.ConfigParser()
        parser.optionxform = str
        parser.readfp(open(old_desktop_file, 'r'))
        parser.set('Desktop Entry', 'Hidden', 'true')
        parser.set('Desktop Entry', 'X-GNOME-Autostart-enabled', 'false')
        parser.set('Desktop Entry', 'X-MATE-Autostart-enabled', 'false')
        with open(new_desktop_file, 'w') as fp:
            parser.write(fp)
