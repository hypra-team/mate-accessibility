#!/usr/bin/python
from gi.repository import Caja, GObject
import os
import subprocess
import gettext

class OcrizeExtension(GObject.GObject, Caja.MenuProvider):
    def __init__(self):
        gettext.bindtextdomain('mate-accessibility')
        gettext.textdomain('mate-accessibility')

    def ocrize(self, menu, files, output=None):
        files_names = []
        for fil in files:
            filename = fil.get_location().get_path()
            if filename is not None:
                files_names.append(filename)
        if not files_names:
            return
        path = os.path.split(files_names[0])[0]
        arguments = ["ocrizer", "-d", path]
        if output != None:
            arguments.append("-o")
            arguments.append(output)
        arguments.extend(files_names)
        subprocess.Popen(arguments)

    def ocrize_html(self, menu, files):
        self.ocrize(menu, files, "htmlversion10defaults")

    def check_extensions(self, files):
        extensions = '.pdf', '.png', '.jpg', '.jpeg', '.gif', '.bmp', '.tiff', '.dcx', '.jbig2'
        for fil in files:
            if not fil.get_name().lower().endswith(extensions):
                return False
            if fil.get_location().get_path() is None:
                return False
        return True

    def check_html_supported(self):
        """ Check whether HTML conversion is supported, which is only the case
        if using either Finereader or Online backends """

        # Finereader
        if (os.path.isfile('/usr/local/bin/abbyyocr11') and
            os.access('/usr/local/bin/abbyyocr11', os.X_OK)):
            return True
        # Online
        if os.access('/etc/ocrizer/online/id_rsa', os.R_OK):
            return True
        return False

    def get_file_items(self, window, files):
        if not self.check_extensions(files):
            return
        ocrize = Caja.MenuItem(
            name="Ocrize",
            label=gettext.dgettext("mate-accessibility", "Launch Optical Character Recognition")
        )
        ocrize.connect('activate', self.ocrize, files)

        if self.check_html_supported():
            ocrize_html = Caja.MenuItem(
                name="OcrizeHTML",
                label=gettext.dgettext("mate-accessibility", "Launch Optical Character Recognition (HTML)")
            )
            ocrize_html.connect('activate', self.ocrize_html, files)
            return [ocrize, ocrize_html]
        return [ocrize]
